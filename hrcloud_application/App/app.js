﻿// glavni angularjs modul za aplikaciju
var myApp = angular.module('myApp', ['ngRoute']);

// postavke preusmjeravanja za ng-view
myApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/songs/', {
        controller: 'songsController',
        templateUrl: '/App/views/songs.html'
    }).when('/playlists/', {
        controller: 'playlistsController',
        templateUrl: '/App/views/playlists.html'
    }).when('/songs/search/', {
        controller: 'songsController',
        templateUrl: '/App/views/songs.html'
    }).when('/playlists/search/', {
        controller: 'playlistsController',
        templateUrl: '/App/views/playlists.html'
    }).when('/songs/search/:query', {
        controller: 'songsController',
        templateUrl: '/App/views/songs.html'
    }).when('/playlists/search/:query', {
        controller: 'playlistsController',
        templateUrl: '/App/views/playlists.html'
    }).when('/songs/details/:id', {
        controller: 'songsDetailsController',
        templateUrl: '/App/views/songsDetails.html'
    }).when('/playlists/details/:id', {
        controller: 'playlistsDetailsController',
        templateUrl: '/App/views/playlistsDetails.html'
    }).otherwise({
        redirectTo: '/songs/'
    });
}]);