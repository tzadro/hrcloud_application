﻿// kontroler za stranicu sa svim playlistama i za pretrazivanje playlisti
myApp.controller('playlistsController', ['$scope', '$routeParams', 'playlistsService', '$location', function ($scope, $routeParams, playlistsService, $location) {
    // korisnikov upit za pretrazivanje
    var search = $routeParams.query;
    $scope.query = $routeParams.query;
    if ($location.url() == '/playlists/search/')
        $location.url('/playlists/');
    if (search == null || search == 'undefined')
        search = '';

    // dohvat podataka o playlistama sa servera
    $scope.getPlaylists = function (search) {
        playlistsService.getPlaylists(search)
            .success(function (data) {
                $scope.playlists = data;
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }
    $scope.getPlaylists(search);

    // funkcija za brisanje pjesme iz baze podataka
    $scope.delete = function (title, id) {
        if (confirm("Are you sure you want to delete \"" + title + "\" playlist?")) {
            playlistsService.delete(id)
                .success(function (data) {
                    $scope.getPlaylists(search);
                });
        }
    };

    // funkcija za kreiranje nove pjesme
    $scope.create = function (newName) {
        if (newName == '')
            confirm("Please enter playlist name!");
        else playlistsService.create(newName)
                .success(function (data) {
                    if (data == 'True') {
                        if ($location.url() == '/playlists/') {
                            $scope.newName = '';
                            $scope.getPlaylists(search);
                        } else
                            $location.url('/playlists/');
                    } else if (data == 'False')
                        confirm("There is already a playlist by that name!");
                })
    };
}]);