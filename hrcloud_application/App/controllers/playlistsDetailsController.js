﻿// kontroler za stranicu s detaljem jedne playliste
myApp.controller('playlistsDetailsController', ['$scope', '$routeParams', 'playlistsService', function ($scope, $routeParams, playlistsService) {
    // dohvat podataka o pjesmi sa servera
    $scope.getDetails = function (param) {
        playlistsService.getDetails(param)
            .success(function (data) {
                $scope.playlist = data;
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }
    $scope.getDetails($routeParams.id);

    // funkcija za uklanjanje pjesme iz playliste
    $scope.remove = function (stitle, sid, ptitle, pid) {
        if (confirm("Are you sure you want to remove \"" + stitle + "\" song from \"" + ptitle + "\" playlist?")) {
            playlistsService.remove(sid, pid)
                .success(function (data) {
                    $scope.getDetails($routeParams.id);
                });
        }
    }

    // funkcija za spremanje novog imena playliste
    $scope.save = function (playlist, newName) {
        if (newName == '' || newName == null)
            return;

        playlistsService.edit(playlist.ID, newName)
            .success(function (data) {
                $scope.getDetails(playlist.ID);
            });
    }
}]);