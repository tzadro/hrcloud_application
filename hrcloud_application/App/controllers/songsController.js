﻿// kontroler za stranicu sa svim pjesmama i za pretrazivanje pjesama
myApp.controller('songsController', ['$scope', '$routeParams', 'songsService', '$location', function ($scope, $routeParams, songsService, $location) {
    // korisnikov upit
    var search = $routeParams.query;
    $scope.query = $routeParams.query;
    if ($location.url() == '/songs/search/')
        $location.url('/songs/');
    if (search == null || search == 'undefined')
        search = '';

    // azuriranje podataka o pjesamam ovisno o upitu
    $scope.getSongs = function (search) {
        songsService.getSongs(search)
            .success(function (data) {
                $scope.songs = data;
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }
    $scope.getSongs(search);

    // funkcija za brisanje pjesme iz baze podataka
    $scope.delete = function (title, id) {
        if (confirm("Are you sure you want to delete \"" + title + "\" song?")) {
            songsService.delete(id)
                .success(function (data) {
                    $scope.getSongs(search);
                });
        }
    };

    // funkcija za stvaranje nove pjesme
    $scope.create = function (newTitle, newArtist) {
        if (newTitle == '' || newTitle == null)
            confirm("Please enter song title!");
        else if (newArtist == '' || newArtist == null)
            confirm("Please enter song artist!");
        else songsService.create(newTitle, newArtist)
                .success(function (data) {
                    if (data == 'True') {
                        if ($location.url() == '/songs/') {
                            $scope.newTitle = '';
                            $scope.newArtist = '';
                            $scope.getSongs(search);
                        } else
                            $location.url('/songs/');
                    } else if (data == 'False')
                        confirm("That song already exists!");
                })
    };
}]);