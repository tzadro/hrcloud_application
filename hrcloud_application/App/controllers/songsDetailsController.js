﻿// kontroler za stranicu s detaljem jedne pjesme
myApp.controller('songsDetailsController', ['$scope', '$routeParams', 'songsService', function ($scope, $routeParams, songsService) {
    // dohvat podataka o pjesmi sa servera
    $scope.updateView = function (sid) {
        songsService.getDetails($routeParams.id)
        .success(function (data) {
            $scope.song = data;
        })
        .error(function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
            console.log($scope.status);
        });
    }

    // dohvat podataka o pjesmi i svim postojecim playlistama
    songsService.getDetails($routeParams.id)
        .success(function (data) {
            $scope.song = data;

            songsService.getPlaylists()
                .success(function (data) {
                    $scope.playlists = data;

                    update($scope.playlists, $scope.song.Playlists);
                })
                .error(function (error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                    console.log($scope.status);
                });
        })
        .error(function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
            console.log($scope.status);
        });

    // funkcija koja oznaci sve playliste u kojima se nalazi pjesma
    var update = function (param1, param2) {
        for (var i in param1) {
            for (var j in param2) {
                if (param1[i].ID == param2[j].ID)
                    param1[i].confirmed = true;
            }
        }
    };

    // funkcija za dodavanje/micanje pjesme iz odredene playliste
    $scope.change = function (confirmed, pid, sid) {
        if (confirmed) {
            songsService.add(sid, pid);
        } else {
            songsService.remove(sid, pid);
        }
    }

    // funkcija za spremanje izmjene naziva i/ili autora pjesme
    $scope.save = function (song, newTitle, newArtist) {
        if (newTitle == '' || newTitle == null)
            newTitle = song.Title;
        if (newArtist == '' || newArtist == null)
            newArtist = song.Artist;

        songsService.edit(song.ID, newTitle, newArtist)
            .success(function (data) {
                $scope.updateView(song.ID);
            });
    }
}]);