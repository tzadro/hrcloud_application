﻿// service za komunikaciju izmedju angularjsovog playlistsControllera i asp.netovog mvc PlaylistControllera
myApp.factory('playlistsService', ['$http', function ($http) {
    var playlistsService = {};

    playlistsService.getPlaylists = function (param) {
        return $http.get('/Playlists/Index/' + param);
    }

    playlistsService.getDetails = function (param) {
        return $http.get('/Playlists/Details/' + param);
    }

    playlistsService.delete = function (param) {
        return $http.get('/Playlists/DeleteConfirmed/' + param);
    }

    playlistsService.remove = function (param1, param2) {
        return $http.get('/Navigations/DeleteConfirmed/?sid=' + param1 + '&pid=' + param2);
    }

    playlistsService.create = function (param) {
        return $http.get('/Playlists/Create/?newName=' + param);
    }

    playlistsService.edit = function (param1, param2) {
        return $http.get('/Playlists/Edit/?pid=' + param1 + '&newName=' + param2);
    }

    return playlistsService;
}])