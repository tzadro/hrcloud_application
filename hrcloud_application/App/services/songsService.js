﻿// service za komunikaciju izmedju angularjsovog songsControllera i asp.netovog mvc SongsControllera
myApp.factory('songsService', ['$http', function ($http) {
    var songsService = {};

    songsService.getSongs = function (param) {
        return $http.get('/Songs/Index/' + param);
    }

    songsService.getDetails = function (param) {
        return $http.get('/Songs/Details/' + param);
    }

    songsService.delete = function (param) {
        return $http.get('/Songs/DeleteConfirmed/' + param);
    }

    songsService.remove = function (param1, param2) {
        return $http.get('/Navigations/DeleteConfirmed/?sid=' + param1 + '&pid=' + param2);
    }

    songsService.add = function (param1, param2) {
        return $http.get('/Navigations/Create/?sid=' + param1 + '&pid=' + param2);
    }

    songsService.create = function (param1, param2) {
        return $http.get('/Songs/Create/?newTitle=' + param1 + '&newArtist=' + param2);
    }

    songsService.edit = function (param1, param2, param3) {
        return $http.get('/Songs/Edit/?sid=' + param1 + '&newTitle=' + param2 + '&newArtist=' + param3);
    }

    songsService.getPlaylists = function () {
        return $http.get('/Playlists/Index/');
    }

    return songsService;
}])