﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using hrcloud_application.DAL;
using hrcloud_application.Models;

// kontroler za komunikaciju s tablicom iz baze podataka u kojoj se spremaju podaci o tome koja pjesma se nalazi u kojoj playlisti
namespace hrcloud_application.Controllers
{
    public class NavigationsController : Controller
    {
        private PlayerContext db = new PlayerContext();

        // POST: Navigations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public bool Create(int sid, int pid)
        {
            var navigation = new Navigation { SongID = sid, PlaylistID = pid };

            foreach (Navigation e in db.Navigations)
            {
                if ((e.SongID == sid) && (e.PlaylistID == pid))
                    return false;
            }

            if (ModelState.IsValid)
            {
                db.Navigations.Add(navigation);
                db.SaveChanges();
                return true;
            }

            return false;
        }
        
        // POST: Navigations/Delete/5
        public bool DeleteConfirmed(int sid, int pid)
        {
            var navigations = db.Navigations.Include(n => n.Playlist).Include(n => n.Song);
            foreach (Navigation e in navigations)
            {
                if (e.SongID == sid && e.PlaylistID == pid)
                {
                    db.Navigations.Remove(e);
                }
            }
            db.SaveChanges();
            return true;
        }
    }
}
