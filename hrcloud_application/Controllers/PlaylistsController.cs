﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using hrcloud_application.DAL;
using hrcloud_application.Models;

// kontroler za komunikaciju s bazom podataka (tablica s podacima o playlistama)
namespace hrcloud_application.Controllers
{
    public class PlaylistsController : Controller
    {
        private PlayerContext db = new PlayerContext();

        // GET: Playlists
        public ActionResult Index(string id)
        {
            string searchString = id;
            var playlists = from m in db.Playlists
                        select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                playlists = playlists.Where(s => s.Name.Contains(searchString));
            }

            var result = playlists.Select(it => new { ID = it.ID, Name = it.Name }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Playlists/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Playlist playlist = db.Playlists.Find(id);
            if (playlist == null)
            {
                return HttpNotFound();
            }

            CompletePlaylist result = new CompletePlaylist { ID = playlist.ID, Name = playlist.Name };
            result.Songs = playlist.Navigations.Select(it => it.Song).ToList().Select(it => new SimpleSong { ID = it.ID, Title = it.Title, Artist = it.Artist }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // POST: Playlists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public bool Create(string newName)
        {
            var playlist = new Playlist { Name = newName };

            foreach (Playlist e in db.Playlists)
            {
                if (String.Equals(e.Name, playlist.Name, StringComparison.CurrentCultureIgnoreCase))
                    return false;
            }

            if (ModelState.IsValid)
            {
                db.Playlists.Add(playlist);
                db.SaveChanges();
                return true;
            }

            return false;
        }

        // POST: Playlists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public bool Edit(int pid, string newName)
        {
            Playlist playlist = new Playlist { ID = pid, Name = newName };

            if (ModelState.IsValid)
            {
                db.Entry(playlist).State = EntityState.Modified;
                db.SaveChanges();
                return true; ;
            }
            return false;
        }

        // POST: Playlists/Delete/5
        public bool DeleteConfirmed(int id)
        {
            Playlist playlist = db.Playlists.Find(id);
            db.Playlists.Remove(playlist);
            db.SaveChanges();
            return true;
        }
    }
}
