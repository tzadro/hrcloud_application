﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using hrcloud_application.DAL;
using hrcloud_application.Models;

// kontroler za komunikaciju s bazom podataka (tablica s podacima o pjesmama)
namespace hrcloud_application.Controllers
{
    public class SongsController : Controller
    {
        private PlayerContext db = new PlayerContext();

        // GET: Songs
        public ActionResult Index(string id)
        {
            string searchString = id;
            var songs = from m in db.Songs
                        select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                songs = songs.Where(s => s.Title.Contains(searchString) || s.Artist.Contains(searchString));
            }

            var result = songs.Select(it => new SimpleSong { ID = it.ID, Title = it.Title, Artist = it.Artist }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Songs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Song song = db.Songs.Find(id);
            if (song == null)
            {
                return HttpNotFound();
            }

            CompleteSong result = new CompleteSong { ID = song.ID, Title = song.Title, Artist = song.Artist };
            result.Playlists = song.Navigations.Select(it => it.Playlist).ToList().Select(it => new SimplePlaylist { ID = it.ID, Name = it.Name }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // POST: Songs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public bool Create(string newTitle, string newArtist)
        {
            var song = new Song { Title = newTitle, Artist = newArtist };

            foreach (Song e in db.Songs)
            {
                if (String.Equals(e.Title, song.Title, StringComparison.CurrentCultureIgnoreCase)
                    && String.Equals(e.Artist, song.Artist, StringComparison.CurrentCultureIgnoreCase))
                    return false;
            }

            if (ModelState.IsValid)
            {
                db.Songs.Add(song);
                db.SaveChanges();
                return true;
            }

            return false;
        }
        
        // POST: Songs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public bool Edit(int sid, string newTitle, string newArtist)
        {
            Song song = new Song { ID = sid, Title = newTitle, Artist = newArtist };

            if (ModelState.IsValid)
            {
                db.Entry(song).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            return false;
        }

        // POST: Songs/Delete/5
        public bool DeleteConfirmed(int id)
        {
            Song song = db.Songs.Find(id);
            db.Songs.Remove(song);
            db.SaveChanges();
            return true;
        }
    }
}
