﻿using hrcloud_application.Models;
using System;
using System.Data.Entity;

// entity frameworkov context za bazu podataka
namespace hrcloud_application.DAL
{
    public class PlayerContext : DbContext
    {
        public DbSet<Song> Songs { get; set; }
        public DbSet<Navigation> Navigations { get; set; }
        public DbSet<Playlist> Playlists { get; set; }
    }
}