﻿using hrcloud_application.Models;
using System;
using System.Collections.Generic;

// inicijaliziranje baze podataka, trenutno postavljeno da dropa i ponovo kreira bazu svaki put kada se model promijeni
namespace hrcloud_application.DAL
{
    // public class PlayerInitializer : System.Data.Entity.DropCreateDatabaseAlways<PlayerContext>
    public class PlayerInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<PlayerContext>
    {
        protected override void Seed(PlayerContext context)
        {
            var songs = new List<Song>
            {
				new Song{Title="505",Artist="Arctic Monkeys"},
				new Song{Title="Something's Got a Hold On Me",Artist="Etta James"},
				new Song{Title="Budapest",Artist="George Ezra"},
				new Song{Title="Crazy In Love",Artist="Beyonce"},
				new Song{Title="Africa",Artist="Toto"},
				new Song{Title="Scar Tissue",Artist="Red Hot Chili Peppers"},
				new Song{Title="Moje Leto",Artist="IN VIVO ft. Boyant"},
				new Song{Title="The Scientist",Artist="Coldplay"},
				new Song{Title="Space Bound",Artist="Eminem"},
				new Song{Title="Sugar",Artist="Maroon 5"},
				new Song{Title="What It's Like",Artist="Everlast"},
				new Song{Title="Lateralus",Artist="Tool"},
				new Song{Title="Self Esteem",Artist="The Offspring"},
                new Song{Title="Volim Piti i Ljubiti",Artist="Vuco"},
				new Song{Title="Unintended",Artist="Muse"},
            };
            songs.ForEach(s => context.Songs.Add(s));
            context.SaveChanges();

            var playlists = new List<Playlist>
            {
                new Playlist{Name="day1"},
                new Playlist{Name="DJTM"},
                new Playlist{Name="Chill"},
                new Playlist{Name="Feeling Lucky"},
                new Playlist{Name="Pjesnicka Sloboda"},
            };
            playlists.ForEach(s => context.Playlists.Add(s));
            context.SaveChanges();

            var navigations = new List<Navigation>
            {
                new Navigation{SongID=1,PlaylistID=1},
                new Navigation{SongID=5,PlaylistID=1},
                new Navigation{SongID=6,PlaylistID=1},
                new Navigation{SongID=8,PlaylistID=1},
                new Navigation{SongID=13,PlaylistID=1},
                new Navigation{SongID=4,PlaylistID=2},
                new Navigation{SongID=5,PlaylistID=2},
                new Navigation{SongID=8,PlaylistID=2},
                new Navigation{SongID=9,PlaylistID=2},
                new Navigation{SongID=10,PlaylistID=2},
                new Navigation{SongID=15,PlaylistID=2},
                new Navigation{SongID=1,PlaylistID=3},
                new Navigation{SongID=2,PlaylistID=3},
                new Navigation{SongID=3,PlaylistID=3},
                new Navigation{SongID=4,PlaylistID=3},
                new Navigation{SongID=5,PlaylistID=3},
                new Navigation{SongID=7,PlaylistID=3},
                new Navigation{SongID=10,PlaylistID=3},
                new Navigation{SongID=14,PlaylistID=3},
                new Navigation{SongID=15,PlaylistID=3},
                new Navigation{SongID=1,PlaylistID=4},
                new Navigation{SongID=2,PlaylistID=4},
                new Navigation{SongID=5,PlaylistID=4},
                new Navigation{SongID=6,PlaylistID=4},
                new Navigation{SongID=8,PlaylistID=4},
                new Navigation{SongID=9,PlaylistID=4},
                new Navigation{SongID=10,PlaylistID=4},
                new Navigation{SongID=13,PlaylistID=4},
                new Navigation{SongID=15,PlaylistID=4},
                new Navigation{SongID=1,PlaylistID=5},
                new Navigation{SongID=3,PlaylistID=5},
                new Navigation{SongID=5,PlaylistID=5},
                new Navigation{SongID=6,PlaylistID=5},
                new Navigation{SongID=8,PlaylistID=5},
                new Navigation{SongID=9,PlaylistID=5},
                new Navigation{SongID=10,PlaylistID=5},
                new Navigation{SongID=11,PlaylistID=5},
                new Navigation{SongID=13,PlaylistID=5},
                new Navigation{SongID=14,PlaylistID=5},
                new Navigation{SongID=15,PlaylistID=5},
            };
            navigations.ForEach(s => context.Navigations.Add(s));
            context.SaveChanges();
        }
    }
}