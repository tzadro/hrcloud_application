﻿using System;

// model tablice iz baze podataka s podacima o tome koje pjesme se nalaze u kojim playlistama
namespace hrcloud_application.Models
{
    public class Navigation
    {
        public int ID { get; set; }
        public int SongID { get; set; }
        public int PlaylistID { get; set; }

        public virtual Song Song { get; set; }
        public virtual Playlist Playlist { get; set; }
    }
}