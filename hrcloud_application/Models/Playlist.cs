﻿using System;
using System.Collections.Generic;

// model tablice iz baze podataka s podacima o playlistama
namespace hrcloud_application.Models
{
    public class Playlist
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Navigation> Navigations { get; set; }
    }
}