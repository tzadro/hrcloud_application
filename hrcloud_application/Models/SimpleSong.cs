﻿using System;
using System.Collections.Generic;

/* Used in controller to send to angularjs controller, not part of database, made to avoid circular reference */
namespace hrcloud_application.Models
{
    public class SimpleSong
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
    }
}