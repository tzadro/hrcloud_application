﻿using System;
using System.Collections.Generic;

// model tablice iz baze podataka s podacima o pjesmama
namespace hrcloud_application.Models
{
    public class Song
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }

        public virtual ICollection<Navigation> Navigations { get; set; }
    }
}